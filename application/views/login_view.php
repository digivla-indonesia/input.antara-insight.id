<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

Header( "Last-Modified: " . gmdate( "D, j M Y H:i:s" ) . " GMT" );
Header( "Expires: " . gmdate( "D, j M Y H:i:s", time() ) . " GMT" );
Header( "Cache-Control: no-store, no-cache, must-revalidate" ); // HTTP/1.1
Header( "Cache-Control: post-check=0, pre-check=0", FALSE );
Header( "Pragma: no-cache" ); // HTTP/1.0

date_default_timezone_set('Asia/Jakarta');	
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>DIGIVLA - NEWS - INFORMATION - INSIGHT</title>
    <link rel="shortcut icon" href="<?=base_url();?>asset/images/favicon.ico">
    <?php echo $extraHeadContent; ?>
</head>

<body class="grey darken-3"> 
  <!-- Start Page Loading -->
  <div id="loader-wrapper">
      <div id="loader"></div>        
      <div class="loader-section section-left"></div>
      <div class="loader-section section-right"></div>
  </div>
  <!-- End Page Loading --> 
  <div id="login-page" class="row" style="z-index:3;position: relative; opacity:0.7;">
    <div class="col s12 z-depth-4 card-panel">
      <form class="login-form" method="post" action="<?=site_url('auth');?>/login_check" id="lform">
        <div class="row">
          <div class="input-field col s12 center">
            <img src="<?=base_url();?>asset/images/logo.png" alt="" class="  responsive-img valign ">
            <p class="center login-form-text">Back Office</p>
          </div>
        </div>
        <div class="row margin">
          <div class="input-field col s12  ">
            <i class="mdi-social-person-outline prefix grey-text"></i>
            <input id="username" name="luser" type="text">
            <label for="username" class="center-align">Email</label>
          </div>
        </div>
        <div class="row margin">
          <div class="input-field col s12  ">
            <i class="mdi-action-lock-outline prefix grey-text"></i>
            <input id="password" name="lpswd" type="password">
            <label for="password" >Password</label>
          </div>
        </div> 
        <div class="row">
          <div class="input-field col s12">
            <a  id="login-submit" class="btn waves-effect waves-light col s12 grey darken-2 white-text">Login</a>
          </div>
        </div> 

      </form>
    </div>
  </div> 
  <script>
  var form = document.getElementById("lform");

	document.getElementById("login-submit").addEventListener("click", function () {
	  form.submit();
	});
  </script>
</body>
</html>
