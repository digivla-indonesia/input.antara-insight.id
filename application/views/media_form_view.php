<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>

<!-- //////////////////////////////////////////////////////////////////////////// -->

<!-- START CONTENT -->
<section id="content">
	
	<!--breadcrumbs start-->
	<div id="breadcrumbs-wrapper" class=" grey lighten-3">
	  <div class="container">
		<div class="row">
		  <div class="col s12 m12 l12">
			<h5 class="breadcrumbs-title"><?=$title?> </h5>
			<ol class="breadcrumb">
			  <li><a href="<?=site_url('media')?>">Media</a>
			  </li>  
			  <li class="active"> <?=$title?> 
			  </li> 
			</ol>
		  </div>
		</div>
	  </div>
	</div>
	<!--breadcrumbs end--> 
	
	<!--start container-->
	<div class="container">
  

		<div class="row">
			<div class="col s12  ">  
			 
				<div class="content">   
					<form  enctype="multipart/form-data" class="form-horizontal cmxform"  id="formInputData" method="post" action="<?=$url_action?>"  >
					  
					  <div class="row">
						<div class="input-field col s12">
							<label for="media_id" >ID</label> 
							<input id="media_id" name="media_id" type="text" class="validate" value="<?php echo ($act=='create') ? $media_id : $media->media_id; ?>" /> 
						</div>					 
					  </div>
					  <div class="row">
						<div class="input-field col s12">
							<label for="media_name" >Nama (* Format TV = &lt;statisun_tv&gt; - &lt;program_tv&gt; ) &nbsp;&nbsp; ( *Format Radio = &lt;statisun_radio&gt; - &lt;program_radio&gt; ) Cth: NET - NET 5</label> 
							<input id="media_name" name="media_name" type="text" required class="validate" value="<?php echo ($act=='create') ? '' : $media->media_name; ?>" /> 
						</div>					 
					  </div>
					  <div class="row">
							<div class="  col s12">
								<label>Tipe Media</label> 
								<select name="media_type_id" id="media_type_id"    >                     
									<option value="0">Pilih Tipe Media</option>
									
							   <?php
									foreach($media_type as $row){
										if($act == 'edit'){
											if($media->media_type_id == $row->media_type_id){
												echo '<option value="'.$row->media_type_id.'" selected>'.$row->media_type.'</option>';
											}
											else{
												echo '<option value="'.$row->media_type_id.'">'.$row->media_type.'</option>';
											}
										}else{							
											echo '<option value="'.$row->media_type_id.'">'.$row->media_type.'</option>';
										}
									}      
							   ?>
								</select> 
							</div>
					  </div> 
					  
					  <div class="row">
							<div class="input-field col s4">
								<label class="circulation">Circulation</label> 
								<input id="circulation" name="circulation" type="text" required class="validate" value="<?php echo ($act=='create') ? '0' : $media->circulation; ?>" /> 
							</div> 
							<div class="input-field col s4">
								<label class="rate_bw">Rate BW</label> 
								<input id="rate_bw" name="rate_bw" type="text" required class="validate" value="<?php echo ($act=='create') ? '0' : $media->rate_bw; ?>" /> 
							</div> 
							<div class="input-field col s4">
								<label class="rate_fc">Rate FC</label> 
								<input id="rate_fc" name="rate_fc" type="text" required class="validate" value="<?php echo ($act=='create') ? '0' : $media->rate_fc; ?>" /> 
							</div>
					 </div> 
					 <div class="row">
						<div class=" col s12">
							<label for="statuse" >Bahasa</label> 
							<div class="row">
								<div class=" col s3">
								  <input name="language" value="IND" type="radio" id="test3"  <?php echo ($act=='create') ? 'checked' : ( $media->language == 'IND') ? 'checked' : '' ; ?> />
								  <label for="test1">Indonesia</label>
								</div>
								<div class=" col s3">
								  <input name="language" value="ENG" type="radio" id="test4"  <?php echo ($act=='create') ? '' : ( $media->language == 'ENG') ? 'checked' : '' ; ?> />
								  <label for="test2">English</label>
								</div> 
							</div>					 
						</div>					 
					  </div> 
					  <div class="row">
						<div class=" col s12">
							<label for="statuse" >Status</label> 
							<div class="row">
								<div class=" col s3">
								  <input name="statuse" value="A" type="radio" id="test1"  <?php echo ($act=='create') ? 'checked' : ( $media->statuse == 'A') ? 'checked' : '' ; ?> />
								  <label for="test1">Aktif</label>
								</div>
								<div class=" col s3">
								  <input name="statuse" value="N" type="radio" id="test2"  <?php echo ($act=='create') ? '' : ( $media->statuse == 'N') ? 'checked' : '' ; ?> />
								  <label for="test2">Tidak Aktif</label>
								</div> 
							</div>					 
						</div>					 
					  </div>
					  
					  <div class="row">
						<div class="input-field col s12">
							<label for="tier" >Tier</label> 
							<!-- <input id="tier" name="tier" type="text" class="validate" value="<?php echo ($act=='create') ? $tier : $media->tier; ?>" /> -->
							<select id="tier" name="tier">
								<option value="0">Pilih Tier</option>
								<?php
									if($media->tier == 1 or $media->tier == '1'){
										echo '<option value="1" selected>1</option>';
									} else{
										echo '<option value="1">1</option>';
									}
									
									if($media->tier == 2 or $media->tier == '2'){
										echo '<option value="2" selected>2</option>';
									} else{
										echo '<option value="2">2</option>';
									}
									
									if($media->tier == 3 or $media->tier == '3'){
										echo '<option value="3" selected>3</option>';
									} else{
										echo '<option value="3">3</option>';
									}
								?>
							</select>
						</div>					 
					  </div>
					 
					  <div class="row">&nbsp;</div>
					  <div class="row">
						  <div class="input-field col s12"> 
							  <button type="submit" class="btn green">Simpan</button>
							  <button type="reset" class="btn green" id="configreset">Batal</button>
							  <a href="" class="btn grey darken-3 back" style="float:right;">Kembali</a> 
						</div>
					  </div>
					</form> 
					
				</div><!-- End .content -->
			</div> <!-- End box -->
		</div> <!-- End .row-fluid -->
	</div> 
	<!--end container-->
</section>
<!-- END CONTENT -->  
<script type="text/javascript" charset="utf-8">
	 
	$("#formInputData").submit(function(event){  console.log(JSON.stringify($(this).serialize()));
				/* stop form from submitting normally */
				event.preventDefault();  
				  
				$.ajax({
						cache:false,
						type: $(this).attr('method'),
						url: $(this).attr('action'), 
						data: $(this).serialize(), 
						success:function(data) {  
									notif({
										msg: data.message,
										type: data.type,
										width: "all",
										time:1000,
										position:"center"
									});
								if(data.type == "success"){
									 $("#configreset").click();
								 }  
							}
						}),
						error: function(data){
							alert(JSON.stringify(data));
						};
					 
				return false; // required to block normal submit since you used ajax 
		});
		 
		 
		 
    </script>
