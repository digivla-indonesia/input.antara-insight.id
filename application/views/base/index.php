<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
$headbarcolor = ' cyan accent-4';
$sidepanel = "sidepanel"; 
$paramdata['headbarcolor'] = $headbarcolor; 
$this->load->view('base/header',$paramdata); 
$this->load->view('base/'.$sidepanel);
$this->load->view($content); 
$this->load->view('base/footer',$paramdata);

?>
