<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
 
<!-- START LEFT SIDEBAR NAV-->
<aside id="left-sidebar-nav">
	<ul id="slide-out" class="side-nav fixed leftside-navigation  grey lighten-5 ">
		<li class="user-details cyan darken-2">
			<div class="row">
				<div class="col col s4 m4 l4">
					<img src="<?=base_url();?>asset/images/avatar.jpg" alt="" class="circle responsive-img valign profile-image">
				</div>
				<div class="col col s8 m8 l8">
					<ul id="profile-dropdown" class="dropdown-content"> 
						<li><a href="<?=site_url('akun');?>"><i class="mdi-action-account-box"></i> Akun</a>
						</li> 
						<li class="divider"></li> 
						<li><a href="<?=site_url('auth/logout');?>"><i class="mdi-hardware-keyboard-tab"></i> Keluar</a>
						</li>
					</ul>
					<a class="btn-flat dropdown-button waves-effect waves-light white-text profile-btn" href="#" data-activates="profile-dropdown"><?=$this->session->userdata('unm');?><i class="mdi-navigation-arrow-drop-down right"></i></a>
					<p class="user-roal"><?=$this->session->userdata('lnm');?></p>
				</div>
			</div>
		</li>  
		<li class="bold <?=$group_menu=='television'?'active grey white-text':''?>"><a  href="<?=site_url('television');?>" class="waves-effect waves-cyan "><i class="mdi-hardware-desktop-mac"></i> Television</a></li> 
		<li class="bold <?=$group_menu=='radio'?'active grey white-text':''?>"><a  href="<?=site_url('radio');?>" class="waves-effect waves-cyan "><i class="mdi-av-radio"></i> Radio</a></li> 
		<li class="bold <?=$group_menu=='news'?'active grey white-text':''?>"><a  href="<?=site_url('news');?>" class="waves-effect waves-cyan "><i class="mdi-action-description"></i>Online News (Manual)</a></li>  
		<li class="bold <?=$group_menu=='media'?'active grey white-text':''?>"><a  href="<?=site_url('media');?>" class="waves-effect waves-cyan "><i class="mdi-action-perm-media"></i> Media</a></li>  
		<li class="bold <?=$group_menu=='television_lists'?'active grey white-text':''?>"><a  href="<?=site_url('television/lists');?>" class="waves-effect waves-cyan "><i class="mdi-action-view-module"></i> News Lists TV</a></li>  
		<li class="bold <?=$group_menu=='radio_lists'?'active grey white-text':''?>"><a  href="<?=site_url('radio/lists');?>" class="waves-effect waves-cyan "><i class="mdi-action-view-quilt"></i> News Lists Radio</a></li>  
<!-- 		<li class="bold <?=$group_menu=='news_lists'?'active grey white-text':''?>"><a  href="<?=site_url('news/lists');?>" class="waves-effect waves-cyan "><i class="mdi-action-view-list"></i> News Lists Online</a></li>   -->
		
		 	
		<li class="li-hover"><div class="divider"></div></li> 
        
	</ul> 
	<a href="#" data-activates="slide-out" class="sidebar-collapse btn-floating btn-medium waves-effect waves-light hide-on-large-only darken-2"><i class="mdi-navigation-menu"></i></a>
</aside>
<!-- END LEFT SIDEBAR NAV-->
                     
