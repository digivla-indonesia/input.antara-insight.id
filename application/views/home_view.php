<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>


<!-- //////////////////////////////////////////////////////////////////////////// -->

<!-- START CONTENT -->
<section id="content">
	
	<!--breadcrumbs start-->
	<div id="breadcrumbs-wrapper" class=" grey lighten-3">
	  <div class="container">
		<div class="row">
		  <div class="col s12 m12 l12">
			<h5 class="breadcrumbs-title">Admin Panel</h5>
			<ol class="breadcrumb">
			  <li><a href="<?=site_url('home')?>">Beranda</a>
			  </li>    
			  <li class="active"> Selamat Datang. 
			  </li> 
			</ol>
		  </div>
		</div>
	  </div>
	</div>
	<!--breadcrumbs end--> 
	
	<!--start container-->
	<div class="container">
		 
        
		<div class="divider"></div>
		<div class="row">
            <div class="col s12  "> 
				 <p>
					Selamat Datang <strong><?=$this->session->userdata('unm')?></strong>  ,  
				 </p>
			</div>
		</div>
	
	</div>
	<!--end container-->
</section>
<!-- END CONTENT -->  


