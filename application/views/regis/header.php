<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

Header( "Last-Modified: " . gmdate( "D, j M Y H:i:s" ) . " GMT" );
Header( "Expires: " . gmdate( "D, j M Y H:i:s", time() ) . " GMT" );
Header( "Cache-Control: no-store, no-cache, must-revalidate" ); // HTTP/1.1
Header( "Cache-Control: post-check=0, pre-check=0", FALSE );
Header( "Pragma: no-cache" ); // HTTP/1.0

date_default_timezone_set('Asia/Jakarta');    

?>

<!DOCTYPE html>
<html>
<head> 
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="msapplication-tap-highlight" content="no">
    <meta name="description" content="ID.ENTITAS - Yayasan Entitas Pekerja Indonesia">
    <meta name="keywords" content="ID.ENTITAS - Yayasan Entitas Pekerja Indonesia">
    <title>ID.ENTITAS - Yayasan Entitas Pekerja Indonesia</title>
    <link rel="shortcut icon" href="<?=base_url();?>asset/images/entitas_logo.png">
    <?php echo $extraHeadContent; ?> 
</head> 
<body   > 
 
 <!-- Start Page Loading -->
    <div id="loader-wrapper">
        <div id="loader"></div>        
        <div class="loader-section section-left"></div>
        <div class="loader-section section-right"></div>
    </div>
    <!-- End Page Loading -->
    
    <!-- //////////////////////////////////////////////////////////////////////////// -->
	 
    <!-- START HEADER -->
    <header id="header" class="page-topbar">
        <!-- start header nav-->
        <div class="navbar-fixed">
            <nav class="<?=$headbarcolor;?>">
                <div class="nav-wrapper">
                    <h1 class="logo-wrapper"><a href="<?=site_url('home');?>" class="brand-logo darken-1">
						<img src="<?=base_url();?>asset/images/entitas_logo.png" alt="Logo" style="max-height:40px;"></a> <span class="logo-text">ID.ENTITAS</span></h1>
                    <?php
						if($sub_menu !== 'register')
						{
                    ?>
                    <ul class="right hide-on-med-and-down">
                        <li class="search-out">
                            <input type="text" class="search-out-text">
                        </li>
                        <li>    
                            <a href="javascript:void(0);" class="waves-effect waves-block waves-light show-search"><i class="mdi-action-search"></i></a>                              
                        </li>
                        <li><a href="javascript:void(0);" class="waves-effect waves-block waves-light toggle-fullscreen"><i class="mdi-action-settings-overscan"></i></a>
                        </li> 
                        <li><a href="<?=site_url('auth/logout');?>" class="waves-effect waves-block waves-light  "><i class="mdi-action-exit-to-app red-text" ></i></a>
                        </li> 
                    </ul>
                    <?php
						}
                    ?>
                </div>
            </nav>
        </div>
        <!-- end header nav-->
    </header>
    <!-- END HEADER -->
    
    

    <!-- //////////////////////////////////////////////////////////////////////////// -->
	<?php
		if($sub_menu !== 'register')
		{
	?> 
	<!-- START MAIN -->
    <div id="main">
        <!-- START WRAPPER -->
        <div class="wrapper"> 
	 <?php
		}
	?>
