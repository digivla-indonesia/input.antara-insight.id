<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
 
<!-- START LEFT SIDEBAR NAV-->
<aside id="left-sidebar-nav">
	<ul id="slide-out" class="side-nav fixed leftside-navigation  grey lighten-5 ">
		<li class="user-details cyan darken-2">
			<div class="row">
				<div class="col col s4 m4 l4">
					<img src="<?=base_url();?>asset/images/avatar.jpg" alt="" class="circle responsive-img valign profile-image">
				</div>
				<div class="col col s8 m8 l8">
					<ul id="profile-dropdown" class="dropdown-content"> 
						<li><a href="<?=site_url('akun');?>"><i class="mdi-action-account-box"></i> Akun</a>
						</li> 
						<li class="divider"></li> 
						<li><a href="<?=site_url('auth/logout');?>"><i class="mdi-hardware-keyboard-tab"></i> Keluar</a>
						</li>
					</ul>
					<a class="btn-flat dropdown-button waves-effect waves-light white-text profile-btn" href="#" data-activates="profile-dropdown"><?=$this->session->userdata('unm');?><i class="mdi-navigation-arrow-drop-down right"></i></a>
					<p class="user-roal"><?=$this->session->userdata('lnm');?></p>
				</div>
			</div>
		</li>  
		<li class="bold <?=$group_menu=='peserta'?'active grey white-text':''?>"><a  href="<?=site_url('peserta');?>" class="waves-effect waves-cyan "><i class="mdi-action-account-circle"></i> Peserta</a></li> 
		<li class="bold <?=$group_menu=='feedback'?'active grey white-text':''?>"><a  href="<?=site_url('feedback');?>" class="waves-effect waves-cyan "><i class="mdi-communication-comment"></i> Feedback</a></li> 
		<li class="bold <?=$group_menu=='testimony'?'active grey white-text':''?>"><a  href="<?=site_url('testimony');?>" class="waves-effect waves-cyan "><i class="mdi-communication-textsms"></i> Testimony</a></li> 
		<li class="bold <?=$group_menu=='permohonan'?'active grey white-text':''?>"><a  href="<?=site_url('permohonan');?>" class="waves-effect waves-cyan "><i class="mdi-communication-live-help"></i> Permohonan</a></li>  
		
		 	
		<li class="li-hover"><div class="divider"></div></li> 
        
	</ul> 
	<a href="#" data-activates="slide-out" class="sidebar-collapse btn-floating btn-medium waves-effect waves-light hide-on-large-only darken-2"><i class="mdi-navigation-menu"></i></a>
</aside>
<!-- END LEFT SIDEBAR NAV-->
                     
