<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>


<!-- //////////////////////////////////////////////////////////////////////////// -->

<!-- START CONTENT -->
<section id="content">
	
	<!--breadcrumbs start-->
	<div id="breadcrumbs-wrapper" class=" grey lighten-3">
	  <div class="container">
		<div class="row">
		  <div class="col s12 m12 l12">
			<h5 class="breadcrumbs-title">Radio</h5>
			<ol class="breadcrumb">
			  <li><a href="<?=site_url('home')?>">Home</a>
			  </li> 
			  <li><a href="<?=site_url('radio/lists')?>">Data Berita Radio</a>
			  </li>   
			  <li class="active"> Lihat Data 
			  </li> 
			</ol>
		  </div>
		</div>
	  </div>
	</div>
	<!--breadcrumbs end--> 
	
	<!--start container-->
	<div class="container">
		 
        
		<div class="divider"></div>
		<div class="row">
            <div class="col s12  ">  
				  &nbsp;
            </div>
        </div>
		<div class="row">
            <div class="col s12  "> 
				<!--DataTables example-->
				<div id="table-datatables"> 
				  <div class="row"> 
					<span class="left">
						<a  href="<?=site_url('radio');?>"  class="btn-floating green btn-large waves-effect waves-light tooltipped red darken-2" data-position="right" data-tooltip="Add New"><i class="mdi-content-add"></i></a>
					</span>
				  </div> 
				  <div class="row"> 
					<table  cellspacing="0" class=" display"  id="table-tv" width="100%">
						<thead>
							<tr>								
								<th width="50">ID</th> 
								<th>Media</th>
								<th>Judul</th> 
								<th>Penyiar Berita</th> 
								<th>Tanggal</th> 
								<th>Jam</th> 
								<th>Durasi</th>  
								<th>Article ID</th>  
							</tr>
						</thead>
						<tbody> 
						</tbody>
					</table>  
				  </div>
				</div> 
			</div>
		</div>
	
	</div>
	<!--end container-->
</section>
<!-- END CONTENT -->  
 

<script type="text/javascript" charset="utf-8">

 function InitOverviewDataTable(){
		var tableConfig = {  
			"dom": 'T<"clear">lfrtip',
			"tableTools": {
				"sSwfPath": "<?=base_url()?>/asset/js/plugins/datatables/extensions/TableTools/swf/copy_csv_xls_pdf.swf"
			},
			processing: true,
			serverSide: true, 
			ajax: {
				"url": "<?=site_url('radio')?>/datatable", 
				"type": "POST"
			}, 
			columns: [ 
				{data: "id" }, 
				{data : "media_name"},
				{data : "title"}, 
				{data : "journalist"},
				{data : "datee"},
				{data : "timee"},
				{data : "duration"},
				{data : "article_id"}
			] , 
			"order": [[ 0, "desc" ]],
		   "aoColumnDefs": [   
				], 
				rowCallback: function ( row, data ) {
					// Set the checked state of the checkbox in the table
					$('input.editor-active', row).prop( 'checked', data.active == 1 );
				}
			};
		var oTable = $('#table-tv').DataTable( tableConfig) 
		 $('.tooltipped').tooltip({delay: 50});
	 }  
	 
	 function refreshTable(){
		oTable = $('#table-tv').DataTable(); 		
		oTable.draw(); 
	 } 
	 
	   

		
	 // Datatables
    $(document).ready(function() {
			
		InitOverviewDataTable();  
		
    } );
    </script>
