<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>


<!-- //////////////////////////////////////////////////////////////////////////// -->

<!-- START CONTENT -->
<section id="content">
	
	<!--breadcrumbs start-->
	<div id="breadcrumbs-wrapper" class=" grey lighten-3">
	  <div class="container">
		<div class="row">
		  <div class="col s12 m12 l12">
			<h5 class="breadcrumbs-title">Media</h5>
			<ol class="breadcrumb">
			  <li><a href="<?=site_url('home')?>">Home</a>
			  </li> 
			  <li><a href="<?=site_url('media')?>">Media</a>
			  </li>   
			  <li class="active"> Lihat Data 
			  </li> 
			</ol>
		  </div>
		</div>
	  </div>
	</div>
	<!--breadcrumbs end--> 
	
	<!--start container-->
	<div class="container">
		 
        
		<div class="divider"></div>
		<div class="row">
            <div class="col s12  ">  
				  &nbsp;
            </div>
        </div>
		<div class="row">
            <div class="col s12  "> 
				<!--DataTables example-->
				<div id="table-datatables"> 
				  <div class="row"> 
					<span class="left">
						<a  href="<?=site_url('media/add');?>"  class="btn-floating green btn-large waves-effect waves-light tooltipped red darken-2" data-position="right" data-tooltip="Add New"><i class="mdi-content-add"></i></a>
					</span>
				  </div> 
				  <div class="row"> 
					<table  cellspacing="0" class=" display"  id="table-media" width="100%">
						<thead>
							<tr>								
								<th width="50">ID</th> 
								<th>Nama</th>
								<th>Type</th> 
								<th>Status</th> 
								<th width="130">Actions</th> 
							</tr>
						</thead>
						<tbody> 
						</tbody>
					</table>  
				  </div>
				</div> 
			</div>
		</div>
	
	</div>
	<!--end container-->
</section>
<!-- END CONTENT -->  
 

<script type="text/javascript" charset="utf-8">

 function InitOverviewDataTable(){
		var tableConfig = {  
			"dom": 'T<"clear">lfrtip',
			"tableTools": {
				"sSwfPath": "<?=base_url()?>/asset/js/plugins/datatables/extensions/TableTools/swf/copy_csv_xls_pdf.swf"
			},
			processing: true,
			serverSide: true, 
			ajax: {
				"url": "<?=site_url('media')?>/datatable", 
				"type": "POST"
			}, 
			columns: [ 
				{data: "media_id" }, 
				{data : "media_name"},
				{data : "media_type"}, 
				{data : "statuse"},
				{data : "media_id"}
			] , 
		   "aoColumnDefs": [ {
				  "aTargets": [ 4 ],
				  "mData": "id",
				  "mRender": function ( data, row, full ) { 
					  
					 return  '<a href="<?=site_url('media')?>/edit/'+data+'" class="btn-floating waves-effect waves-light green darken-3 " title="Edit"><i class="mdi-editor-mode-edit"  ></i></a>&nbsp;' 
							+'<a onClick="del('+data+');" class="btn-floating waves-effect waves-light  red accent-4" title="Delete"><i class="mdi-action-delete"  ></i></a>&nbsp;' 
                            
                            ;
					  
				  }
				} ,
				{
				  "aTargets": [ 3 ],
				  "mData": "peserta_stat",
				  "mRender": function ( data, row, full ) { 
					  
					   if(data === 'A'){
							return  '<a class="waves-effect waves-light  btn green">Aktif</a>'; 
					   }else{
							return '<a class="waves-effect waves-light  btn red">Tidak Aktif</a>';
					   } 
				  }
				} 
				], 
				rowCallback: function ( row, data ) {
					// Set the checked state of the checkbox in the table
					$('input.editor-active', row).prop( 'checked', data.active == 1 );
				}
			};
		var oTable = $('#table-media').DataTable( tableConfig) 
		 $('.tooltipped').tooltip({delay: 50});
	 }  
	 
	 function refreshTable(){
		oTable = $('#table-media').DataTable(); 		
		oTable.draw(); 
	 } 
	 
	  
	
	function del(id)
	{  var result = confirm("Are you sure want to delete this Media?");
		if (result==true) {
			//Logic to delete the item	
			$.ajax({
			cache:false,
			type:'GET',
			url:'<?=site_url('media')?>/delete/'+id,  
			success:function(data) { 
					if(data == 'success')
					{
						notif({
							msg: "Success! Your data is being saved.",
							type: "success",
							width: "all",
							time:1000,
							position:"center"
						});   
						refreshTable();
					}
					else
					{														
						notif({
							msg: "Error. Please try again later.",
							type: "warning",
							width: "all",
							time:1000,
							position:"center"
						}); 	 
					}  
				}
			}); 
		}
	} 

		
	 // Datatables
    $(document).ready(function() {
			
		InitOverviewDataTable();  
		
    } );
    </script>
