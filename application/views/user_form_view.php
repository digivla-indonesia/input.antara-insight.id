<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>
 

<!-- //////////////////////////////////////////////////////////////////////////// -->

<!-- START CONTENT -->
<section id="content">
	
	<!--breadcrumbs start-->
	<div id="breadcrumbs-wrapper" class=" grey lighten-3">
	  <div class="container">
		<div class="row">
		  <div class="col s12 m12 l12">
			<h5 class="breadcrumbs-title">Akun Saya</h5>
			<ol class="breadcrumb">
			  <li><a href="<?=site_url('akun')?>">Akun</a>
			  </li>  
			  <li class="active"> Ubah Akun Saya 
			  </li> 
			</ol>
		  </div>
		</div>
	  </div>
	</div>
	<!--breadcrumbs end--> 
	
	<!--start container-->
	<div class="container">
		<div class="row">
			<div class="col s12  ">  			 
				<div class="content">
					<form  enctype="multipart/form-data" class="form-horizontal cmxform"  id="formInputData" method="post" action="<?=$url_action?>"  >
					 
					  <div class="row">
						<div class="input-field col s12">
							<label for="user_nm" >Nama</label> 
							<input id="user_nm" name="user_nm" type="text" required class="validate" value="<?=$user->user_nm; ?>" /> 
						</div>					 
					  </div>
					  <div class="row">
						<div class="input-field col s12">
							<label for="user_email" >Email</label> 
							<input id="user_email" name="user_email" type="text" required class="validate" value="<?=$user->user_email; ?>" /> 
						</div>					 
					  </div> 
					  <div class="row">
						<div class="input-field col s12">
							<label for="user_pass" >Password Lama</label> 
							<input id="user_pass" name="user_pass" type="password"   class="validate" value="" /> 
						</div>					 
					  </div>
					  <div class="row">
						<div class="input-field col s12">
							<label for="password" >Password Baru</label> 
							<input id="password" name="password" type="password"   class="validate" value="" /> 
						</div>					 
					  </div>
					  <div class="row">
						<div class="input-field col s12">
							<label for="confirm_password" >Konfirmasi Password Baru</label> 
							<input id="confirm_password" name="confirm_password" type="password"   class="validate" value="" /> 
						</div>					 
					  </div>
					  <div class="row">
						  <div class="input-field col s12"> 
							  <button type="submit" class="btn green">Simpan</button>
							  <button type="reset" class="btn green" id="configreset">Batal</button>
							  <a href="" class="btn grey darken-3 back" style="float:right;">Kembali</a> 
						</div>
					  </div>
					</form> 
				</div> <!--end content-->
			</div> <!--end s12-->
		</div> <!--end row-->					 
	</div> 
	<!--end container-->
</section>
<!-- END CONTENT -->   
<script type="text/javascript" charset="utf-8">
	// File Input 
		
	 
    // Datatables
    $(document).ready(function() { 
		 
		$("#formInputData").validate({
			 rules: {
			  user_nm: {
				required: true,
			  },
			  user_email: {
				required: true,
			  }, 
			  user_pass: {
				required: true,
				minlength: 5
			  },
			  password: {
				required: true,
				minlength: 5
			  },
			  confirm_password: {
				required: true,
				minlength: 5,
				equalTo: "#password"
			  }  
			},
			messages: { 
			  user_nm: {
				required: "Silahkan masukkan  Nama",
				minlength: "Minimal 2 karakter"
			  },
			  user_email: {
				required: "Silahkan masukkan  email",
				minlength: "Minimal 2 karakter"
			  }, 
			  user_pass: {
				required: "Silahkan masukkan password lama",
				minlength: "Minimal 5 karakter"
			  },
			  password: {
				required: "Silahkan masukkan password baru",
				minlength: "Minimal 5 karakter"
			  },
			  confirm_password: {
				required: "Silahkan masukkan konfirmasi password baru",
				minlength: "Minimal 5 karakter",
				equalTo: "Silahkan masukkan password yang sama dengan diatas"
			  }  
			} ,
			submitHandler: function (form) {  				
				$.ajax({
						cache:false,
						type: $(form).attr('method'),
						url: $(form).attr('action'),
						data:$(form).serialize(), 
						success:function(data) { 
								if(data == 'success')
								{
									notif({
										msg: "Data yang anda input telah berhasil disimpan.",
										type: "success",
										width: "all",
										time:1000,
										position:"center"
									});
									$("#configreset").click();  
									
								}  
								else if(data == 'wrongpass')
								{														
									notif({
										msg: "Password lama anda salah.",
										type: "warning",
										width: "all",
										time:1000,
										position:"center"
									}); 	 
								}  
								else
								{														
									notif({
										msg: "Error. Please try again later.",
										type: "warning",
										width: "all",
										time:1000,
										position:"center"
									}); 	 
								}  
							}
						});
				return false; // required to block normal submit since you used ajax
			} 
			
		});  
		 
		 
    } );
    </script>
