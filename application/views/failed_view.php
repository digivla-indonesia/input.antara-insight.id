<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>
  

<!-- //////////////////////////////////////////////////////////////////////////// -->

<!-- START CONTENT -->
<section id="content">
	
	<!--breadcrumbs start-->
	<div id="breadcrumbs-wrapper" class=" grey lighten-3">
	  <div class="container">
		<div class="row">
		  <div class="col s12 m12 l12">
			<h5 class="breadcrumbs-title">Pendaftaran Peserta</h5>
			<ol class="breadcrumb">
			  <li><a href="<?=site_url('register')?>">Pendaftaran</a>
			  </li>  
			  <li class="active"> Peserta Baru 
			  </li> 
			</ol>
		  </div>
		</div>
	  </div>
	</div>
	<!--breadcrumbs end--> 
	
	<!--start container-->
	<div class="container"> 
		<div class="row">
			<div class="col s12  ">  
				<div class="title"> 
					<h3>
						<i class=" icon-save"></i> <span><?=ucfirst($title);?></span>
					</h3> 
				</div><!-- End .title --> 
			</div> <!-- End row -->
		</div> <!-- End .row-fluid --> 
		
		<div class="row">
			<div class="col s12 ">
				<p>Maaf, Permintaan anda untuk menjadi Peserta/Anggota ID.ENTITAS tidak berhasil.</p> 
				<p>Silahkan hubungi Customer Service kami untuk melakukan konfirmasi pendaftaran</p>
			</div> 
		</div>
		
	</div>
	<!--end container-->
</section>
<!-- END CONTENT --> 
