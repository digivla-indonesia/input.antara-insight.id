<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


Class Dtable_Media_model extends MY_Model implements DatatableModel
{
	 
		public function appendToSelectStr() {
			return NULL;

        }

        public function fromTableStr() {
            return 'tb_media a';
        } 

        public function joinArray(){
            return array(
	    	  'tb_media_type b' => 'b.media_type_id = a.media_type_id' 
              );
        }

        public function whereClauseArray(){
             return NULL;
        }
	
}

?>
