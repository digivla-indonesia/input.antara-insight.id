<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


Class General_model extends CI_Model { 
	 
	function getAnggota($limit){
		$this->db->select('a.`anggota_id`,a.`anggota_pic`,a.`anggota_name`, n.`negara_name` ');
		$this->db->from('m_anggota a'); 
		$this->db->join('m_negara n','a.negara_id = n.negara_id');  
		$this->db->order_by("a.`anggota_id`","DESC");   
		$this->db->limit($limit);  
		$query = $this->db->get();
		
		return $query;
	}
	function getBagihasil($limit){
		$this->db->select('anggota_id ,  anggota_name ,   bh_amount , bh_time');
		$this->db->from('t_bagihasil');  
		$this->db->order_by("bh_id","DESC");   
		$this->db->limit($limit);  
		$query = $this->db->get();
		
		return $query;
	}
 
}

?>
