<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


Class Dtable_Training_model extends MY_Model implements DatatableModel
{
	 
		public function appendToSelectStr() {
			return NULL;

        }

        public function fromTableStr() {
            return 't_training a';
        } 

        public function joinArray(){
            return array(
	    	  'p_kecamatan kec' => 'kec.kecamatan_id = a.kecamatan_id', 
	    	  'p_kota kot' => 'kot.kota_id = kec.kota_id', 
	    	  'm_provinsi prov' => 'prov.provinsi_id = kec.provinsi_id', 
	    	  't_training_type tipe' => 'tipe.training_type_id = a.training_type_id', 
              );
        }

        public function whereClauseArray(){
             return NULL;
        }
	
}

?>
