<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


Class Dtable_Training_Join_model extends MY_Model implements DatatableModel
{
	 
		public function appendToSelectStr() {
			return NULL;

        }

        public function fromTableStr() {
            return 't_training_join a';
        } 

        public function joinArray(){
            return array(
	    	  'm_peserta p' => 'p.peserta_id = a.peserta_id',  
	    	  't_training t' => 't.training_id = a.training_id', 
              );
        }

        public function whereClauseArray(){
             return NULL;
        }
	
}

?>
