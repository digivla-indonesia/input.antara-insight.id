<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


Class Dtable_Radio_model extends MY_Model implements DatatableModel
{
	 
		public function appendToSelectStr() {
			return NULL;

        }

        public function fromTableStr() {
            return 'tb_articles_radio a';
        } 

        public function joinArray(){
            return array(
	    	  'tb_media b' => 'b.media_id = a.media_id' 
              );
        }

        public function whereClauseArray(){
             return NULL;
        }
	
}

?>
