<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); 
/***************************************************************************************
NAME        : Home
PURPOSE     : -
REVISION    : -
***************************************************************************************/ 

Class Home extends MY_Controller {

    public function __construct() {
        parent::__construct();
         
        
        $arrayCSS = array (						
                        "asset/css/materialize.css",
						"asset/css/style.css",   
						"asset/js/plugins/perfect-scrollbar/perfect-scrollbar.css", 
						"asset/css/jquery.pagepiling.css", 
                    );
        
        $arrayJS = array (   
                        "asset/js/jquery-1.11.2.min.js",   

						"asset/js/materialize.min.js",
						 
						"asset/js/prism.js", 

						"asset/js/plugins/perfect-scrollbar/perfect-scrollbar.min.js", 
						"asset/js/plugins.js",
						"asset/js/jquery.pagepiling.js",
						  
                    );
        
        $data['extraHeadContent'] = '';
        
        foreach ($arrayCSS as $css):
            $data['extraHeadContent'] .= '<link rel="stylesheet" href="'.base_url().$css.'">'.PHP_EOL;
        endforeach;
        
        foreach ($arrayJS as $js):
            $data['extraHeadContent'] .= '<script src="'.base_url().$js.'"></script>'.PHP_EOL;
        endforeach;
        $data['sub_menu'] = 'home';
        $data['group_menu'] = 'home';
        $this->load->vars($data);
    }

    public function index() {  
		
		$data['content']    = 'home_view';
		$data['statPage']   = 'home';
		$data['subPage']    = 'main';
        $this->load->view('base/index', $data);        
       
    } 
	
} ?>
