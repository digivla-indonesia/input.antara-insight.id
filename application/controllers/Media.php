<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); 
/***************************************************************************************
NAME        : Media
PURPOSE     : -
REVISION    : -
***************************************************************************************/ 

Class Media extends MY_Controller {

    public function __construct() {
        parent::__construct();
        
        $this->load->model('media_model', 'media'); 
        $this->load->model('media_type_model', 'media_type'); 
        $this->load->model('user_model', 'user');      
        
        $arrayCSS = array (						 
                        "asset/css/materialize.css",
						"asset/css/style.css",   
						"asset/js/plugins/perfect-scrollbar/perfect-scrollbar.css", 
						
                        'asset/css/notifIt.css',  
						'asset/css/jquery.dataTables.min.css',  
                        'asset/js/plugins/data-tables/css/jquery.dataTables.min.css', 
                        'asset/css/dataTables.tableTools.css',
                        'asset/css/materialize-tags.css',
                    );
        
        $arrayJS = array (   
                        "asset/js/jquery-1.11.2.min.js",   

						"asset/js/materialize.min.js",
						 
						"asset/js/prism.js", 

						"asset/js/plugins/perfect-scrollbar/perfect-scrollbar.min.js", 
						"asset/js/plugins.js", 
						"asset/js/notifIt.js",
						
						'asset/js/plugins/datatables/js/jquery.dataTables.js',
						'asset/js/plugins/datatables/js/dataTables.tableTools.js', 
						'asset/js/materialize-tags.js', 
						  
                    );
        
        $data['extraHeadContent'] = '';
        
        foreach ($arrayCSS as $css):
            $data['extraHeadContent'] .= '<link rel="stylesheet" href="'.base_url().$css.'">'.PHP_EOL;
        endforeach;
        
        foreach ($arrayJS as $js):
            $data['extraHeadContent'] .= '<script src="'.base_url().$js.'"></script>'.PHP_EOL;
        endforeach;
         $data['sub_menu'] = 'media';
         $data['group_menu'] = 'media';
         $data['statPage'] = 'media';
        $this->load->vars($data);
    }

    public function index() {  
		 
		$data['content']    = 'media_view';
        $this->load->view('base/index', $data);    
    } 
     
     
	function add($how = '')
	{
		 
		$data['act']			  = 'create';	
		$data['content']   		  =  'media_form_view';
		
		$data['status'] 		  = 'add_media';
		$data['title'] 		  	  = 'Tambah Media'; 
		$data['act']			  = 'create';	
		$data['url_action']		  = site_url('media/insert');
		
		$data['media_type'] = $this->media_type->get_all(); 
		 
		  
		$medid  = 0;
		$cekMedia		= $this->media->get_all();		
		foreach($cekMedia as $row){
			$medid 			= $row->media_id; 
		}		
		//$nextCode		= $medid + 1;  
		$nextCode		= $medid + 2;  
		$data['media_id'] = $nextCode;
		
		$this->load->view('base/index', $data);
	} 
	
	function edit($id,$how= '')
	{
		 
		
		$data['status'] 		  = 'media'; 
		$data['title'] 		  	  = 'Edit Data Media';
		$data['content']   		  =  'media_form_view'; 
		$data['act']			  = 'edit';	
		$data['id']			  	  = $id;	
		$data['url_action']		  = site_url('media/update/'.$id); 
		
		$data['media_type'] = $this->media_type->get_all(); 
		 
		$media		  		= $this->media->get($id);
		$data['media']		= $media;
		 
		
		$this->load->view('base/index', $data);
	}
	
	function insert(){ 
		 
		$media_id		=  $this->input->post('media_id');
		$media_name		=  $this->input->post('media_name');
		$media_type_id	=  $this->input->post('media_type_id');
		$circulation	=  $this->input->post('circulation');
		$rate_bw		=  $this->input->post('rate_bw');
		$rate_fc		=  $this->input->post('rate_fc');
		$language		=  $this->input->post('language');
		$statuse		=  $this->input->post('statuse'); 
		$tier			=  $this->input->post('tier');
		
		 
		$user_id  = $this->session->userdata('uid');
		$createAt	    	= date("Y-m-d H:i:s");
		$hasil 				= $this->media->insert(array(
								'media_id' => $media_id,
								'media_name' => $media_name,
								'media_type_id' => $media_type_id,
								'circulation' => $circulation,
								'rate_bw' => $rate_bw,
								'rate_fc' => $rate_fc,
								'language' => $language,
								'usere' => $user_id,
								'input_date' => $createAt,
								'tier' => $tier
							)); 
		//echo $hasil;
		
		$data_insert =  array(   
			'media_id' => $media_id,
			'media_name' => $media_name,
			'media_type_id' => $media_type_id,
			'circulation' => $circulation,
			'rate_bw' => $rate_bw,
			'rate_fc' => $rate_fc,
			'language' => $language,
			'usere' => $user_id,
			'input_date' => $createAt,
			'tier' => $tier
		);
		$insertApi 		= $this->site_sentry->insert_media($data_insert);
		print_r($insertApi);
		
		//if($hasil == 0){ 
			 $out = array(
				'type'=>'success',
				'message'=>'Terima Kasih. Media telah berhasil disimpan.'
			);
		/*}else {
			$out = array(
				'type'=>'warning',
				'message'=>'Maaf. Penambahan media tidak berhasil kami simpan.('.$hasil.')'
			); 
		}*/
		
		$this -> output -> set_header("Pragma: no-cache");
        $this -> output -> set_header("Cache-Control: no-store, no-cache");
        $this -> output -> set_content_type('application/json') -> set_output(json_encode($out));
	}
	
	function update($id){
		  
		$media_name		=  $this->input->post('media_name');
		$media_type_id	=  $this->input->post('media_type_id');
		$circulation	=  $this->input->post('circulation');
		$rate_bw		=  $this->input->post('rate_bw');
		$rate_fc		=  $this->input->post('rate_fc');
		$language		=  $this->input->post('language');
		$statuse		=  $this->input->post('statuse'); 
		/*HARRY*/
		$user_id  		=  $this->session->userdata('uid');
		$media_id		=  $this->input->post('media_id');
		$tier			=  $this->input->post('tier');
		/*HARRY*/
		
		$modifyAt	    	= date("Y-m-d H:i:s");
	 
		$data_update =  array(   
								'media_name' => $media_name,
								'media_type_id' => $media_type_id,
								'circulation' => $circulation,
								'rate_bw' => $rate_bw,
								'rate_fc' => $rate_fc,
								'language' => $language,
								'statuse' => $statuse,
								'usere' => $user_id, 
								'tier' => $tier,
								'media_id' => $media_id
								);
		 
		//$hasil 			= $this->media->update($id,$data_update);
		/*HARRY*/
		$hasil 			= $this->media->update($media_id,$data_update);
		$updateApi 		= $this->site_sentry->update_media($data_update); 
		/*HARRY*/
		 print_r($updateApi);
		if($hasil){ 
			 $out = array(
				'type'=>'success',
				'message'=>'Terima Kasih. Media telah berhasil diubah.'
			);
		}else {
			$out = array(
				'type'=>'warning',
				'message'=>'Maaf. Perubahan media tidak berhasil kami simpan.'
			); 
		}
		
		$this -> output -> set_header("Pragma: no-cache");
        $this -> output -> set_header("Cache-Control: no-store, no-cache");
        $this -> output -> set_content_type('application/json') -> set_output(json_encode($out));
	}
	
	function delete($id)
	{
		 
		$data['status'] 		  = 'media';  
		$data['act']			  = 'delete';	
		$data['id']			  	  = $id;			
		
		$data_delete =  array(   
			'media_id' => $id
		);
		$deleteApi 		= $this->site_sentry->delete_media($data_delete);
		
		$howDelete			  	  = $this->media->delete($id); 
		
		if($howDelete){
			echo "success";
		}else {
			echo "failed";
		}
	}  
	 
	public function datatable(){
		//Important to NOT load the model and let the library load it instead.  
        $this -> load -> library('Datatable', array('model' => 'dtable_media_model'));

        //format array is optional, but shown here for the sake of example
        $json = $this -> datatable -> datatableJson(
            array(
                'a_date_col' => 'date',
                'a_boolean_col' => 'boolean',
                'a_percent_col' => 'percent',
                'a_currency_col' => 'currency'
            )
        ); 

        $this -> output -> set_header("Pragma: no-cache");
        $this -> output -> set_header("Cache-Control: no-store, no-cache");
        $this -> output -> set_content_type('application/json') -> set_output(json_encode($json));

	}
	
} 
?>
