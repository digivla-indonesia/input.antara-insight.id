<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); 

Class Akun extends MY_Controller 
{

	function __construct()
	{
		parent::__construct();
		
		$this->load->model('user_model', 'user');     
		  $arrayCSS = array (						 
                        "asset/css/materialize.css",
						"asset/css/style.css",   
						"asset/js/plugins/perfect-scrollbar/perfect-scrollbar.css", 
						
                        'asset/css/notifIt.css',  
						'asset/css/jquery.dataTables.min.css',  
                        'asset/js/plugins/data-tables/css/jquery.dataTables.min.css', 
                        'asset/css/dataTables.tableTools.css',
                        'asset/css/materialize-tags.css',
                    );
        
        $arrayJS = array (   
                        "asset/js/jquery-1.11.2.min.js",   

						"asset/js/materialize.min.js",
						 
						"asset/js/prism.js", 

						"asset/js/plugins/perfect-scrollbar/perfect-scrollbar.min.js", 
						"asset/js/plugins.js", 
						"asset/js/notifIt.js",
						
						'asset/js/plugins/datatables/js/jquery.dataTables.js',
						'asset/js/plugins/datatables/js/dataTables.tableTools.js', 
						'asset/js/materialize-tags.js', 
						'asset/js/jquery.validate.min.js', 
						  
                    );
        
        $data['extraHeadContent'] = '';
        
        foreach ($arrayCSS as $css):
            $data['extraHeadContent'] .= '<link rel="stylesheet" href="'.base_url().$css.'">'.PHP_EOL;
        endforeach;
        
        foreach ($arrayJS as $js):
            $data['extraHeadContent'] .= '<script src="'.base_url().$js.'"></script>'.PHP_EOL;
        endforeach;
         $data['sub_menu'] = 'profil';
         $data['group_menu'] = 'profil';
         $data['statPage'] = 'profil';
        $this->load->vars($data);
	}
	
	function index()
	{ 
		$data['status'] 		  = 'Akun';
		$data['title'] 		  	  = 'Akun';
		$data['content'] 		  = 'user_form_view'; 
		$data['act']			  = 'create';	
		$id					  	  = $this->session->userdata('uid');  
		$data['url_action']		  = site_url('akun/update/'.$id); 
		$data['user']			  = $this->user->get($id);	
		$this->load->view('base/index', $data);
	} 
	 
	function update($id){
		$hasil  		= 0;
		$user_nm 		= $this->input->post('user_nm');
		$user_email		= $this->input->post('user_email'); 
		$user_pass		= $this->input->post('user_pass'); 
		$password		= $this->input->post('password'); 
		$user			= $this->user->get($id);
		if(md5($user_pass) == $user->user_pass)
		{
		
			$hasil 		= $this->user->update($id, array( 
								'user_nm' => $user_nm, 
								'user_email' => $user_email,   
								'user_pass' => md5($password) 
									));
			if($hasil > 0){
				echo 'success';
			}else {
				echo 'failed';
			}
		}
		else{
			echo 'wrongpass';
		}
		
	}
	 
	 
	

}
 
?>
