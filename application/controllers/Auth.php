<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); 

Class Auth extends MY_Controller {
    
    function __construct() {
        parent::__construct();
        
        $arrayCSS = array (
                        "asset/css/materialize.css",
						"asset/css/style.css",
						"asset/css/page-center.css",
						"asset/css/prism.css",					  
						"asset/js/plugins/perfect-scrollbar/perfect-scrollbar.css",
                    );
        
        $arrayJS = array (
                        "asset/js/jquery-1.11.2.min.js",   

						"asset/js/materialize.min.js",
						"asset/js/prism.js",

						"asset/js/plugins/perfect-scrollbar/perfect-scrollbar.min.js",
						"asset/js/plugins.js",
                    );
        
        $data['extraHeadContent'] = '';
        
        foreach ($arrayCSS as $css):
            $data['extraHeadContent'] .= '<link rel="stylesheet" href="'.base_url().$css.'">'.PHP_EOL;
        endforeach;
        
        foreach ($arrayJS as $js):
            $data['extraHeadContent'] .= '<script src="'.base_url().$js.'"></script>'.PHP_EOL;
        endforeach;
        
        $this->load->vars($data);
    }

    function login() {
        if ($this->gate_keeper->is_logged_in()) {
            $user_level = $this->session->userdata('ulevel');
            redirect('home');
        }else{
                $data['login_msg'] = 'Check Your Input';
                $this->load->view('login_view', $data);
            }
    }
    
    function login_check() {
        $usr_login  = $this->input->post('luser');
        $password   = $this->input->post('lpswd');
        $password   = md5($password);
        $this->db->reconnect();        
        
        if (!empty($usr_login) and !empty($password)) {
            $this->gate_keeper->login_routine($usr_login, $password);
        }else{
            redirect('auth/login');
        }
    }
    
    function login_fail() {
        $data['login_msg'] = $this->lang->line('login_msg_fail');
        $this->load->view('login_view', $data);
    }
    
    function logout() {
        $this->gate_keeper->logout_routine();
    }
    
    function update_session() {
        $keys   = $this->input->post('keys');
        $val    = $this->input->post('val');
        
        $this->session->set_userdata($keys, $val);
        echo $this->session->userdata($keys);
    }
}
 
?>
