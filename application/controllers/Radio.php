<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); 
/***************************************************************************************
NAME        : Radio
PURPOSE     : -
REVISION    : -
***************************************************************************************/ 

Class Radio extends MY_Controller {

    public function __construct() {
        parent::__construct();
        
        $this->load->model('media_model', 'media'); 
        $this->load->model('articles_model', 'articles'); 
        $this->load->model('radio_model', 'radio'); 
        $this->load->model('user_model', 'user');      
        
        $arrayCSS = array (						 
                        "asset/css/materialize.css",
						"asset/css/style.css",   
						"asset/js/plugins/perfect-scrollbar/perfect-scrollbar.css", 
						
                        'asset/css/notifIt.css',  
						'asset/css/jquery.dataTables.min.css',  
                        'asset/js/plugins/data-tables/css/jquery.dataTables.min.css', 
                        'asset/css/dataTables.tableTools.css',
                        'asset/css/materialize-tags.css',
                        'asset/css/jquery-te-1.4.0.css',
                    );
        
        $arrayJS = array (   
                        "asset/js/jquery-1.11.2.min.js",   

						"asset/js/materialize.min.js",
						 
						"asset/js/prism.js", 

						"asset/js/plugins/perfect-scrollbar/perfect-scrollbar.min.js", 
						"asset/js/plugins.js", 
						"asset/js/notifIt.js",
						
						'asset/js/plugins/datatables/js/jquery.dataTables.js',
						'asset/js/plugins/datatables/js/dataTables.tableTools.js', 
						'asset/js/materialize-tags.js', 
						'asset/js/jquery-te-1.4.0.min.js', 
						  
                    );
        
        $data['extraHeadContent'] = '';
        
        foreach ($arrayCSS as $css):
            $data['extraHeadContent'] .= '<link rel="stylesheet" href="'.base_url().$css.'">'.PHP_EOL;
        endforeach;
        
        foreach ($arrayJS as $js):
            $data['extraHeadContent'] .= '<script src="'.base_url().$js.'"></script>'.PHP_EOL;
        endforeach;
         $data['sub_menu'] = 'radio';
         $data['group_menu'] = 'radio';
         $data['statPage'] = 'radio';
        $this->load->vars($data);
    }

    public function index() {  
		 
		$data['act']			  = 'create';	
		$data['content']   		  =  'radio_form_view';
		
		$data['status'] 		  = 'add_artikel_tv';
		$data['title'] 		  	  = 'Input Berita Dari Radio'; 
		$data['act']			  = 'create';	
		$data['url_action']		  = site_url('radio/insert');
		
		$data['media'] = $this->media->get_many_by(array('media_type_id' => 13, 'statuse' => 'A')); 
		  
		$this->load->view('base/index', $data);   
    } 
    
    public function lists() {  
		 
		$data['group_menu']    = 'radio_lists';
		$data['content']    = 'radio_view';
        $this->load->view('base/index', $data);    
    } 
	
	function insert(){ 
		 
		//echo "<pre>".var_export($_POST,1)."</pre>";
		 
		$media_id	=  $this->input->post('media_id');
		$title		=  $this->input->post('title');
		$content	=  $this->input->post('content');
		$journalist	=  $this->input->post('journalist');
		$datee		=  $this->input->post('datee');
		$page		=  $this->input->post('page');
		$mmcol		=  $this->input->post('mmcol');  
		
		$content	= strip_tags($content);
		
		$namefile='';
		if(isset($_FILES['file_pdf']['name']))
		{
			$namefile = basename($_FILES['file_pdf']['name']);
			$type = basename($_FILES['file_pdf']['type']); 
			$ext = pathinfo($namefile, PATHINFO_EXTENSION);
		}
		$stat_upl = true;
		$filename = '';
		if($namefile!='')
		{
			//2016-01-28
			//0123456789
			$tahun 	= substr($datee,0,4);
			$bulan 	= substr($datee,5,2);
			$hari 	= substr($datee,8,2);
			
			if (!file_exists('./media_radio/'.$tahun.'/')) {
				mkdir('./media_radio/'.$tahun.'/', 0777, true);				
			}
			if (!file_exists('./media_radio/'.$tahun.'/'.$bulan.'/')) {
					mkdir('./media_radio/'.$tahun.'/'.$bulan.'/', 0777, true);					
			}
			if (!file_exists('./media_radio/'.$tahun.'/'.$bulan.'/'.$hari.'/')) {
				mkdir('./media_radio/'.$tahun.'/'.$bulan.'/'.$hari.'/', 0777, true);
			}
					
			$media_id_str = str_pad($media_id, 3, 0, STR_PAD_LEFT);
			$time_number = time();
			$filename = ''.$tahun.'-'.$bulan.'-'.$hari.'-013-'.$media_id_str.'-'.$time_number .'.'.$ext.'';
			$folder = './media_radio/'.$tahun.'/'.$bulan.'/'.$hari.'/';
			$stat_upl = move_uploaded_file($_FILES['file_pdf']['tmp_name'], $folder.$filename);
			 
		}
		
		$file_pdf = ($filename == '') ? 'default.mp4' : $filename ; 
		$user_id  = $this->session->userdata('uid');
		$createAt	    	= date("Y-m-d H:i:s");
		$hasil 				= $this->articles->insert(array(
								'media_id' => $media_id,
								'title' => $title,
								'content' => $content,
								'journalist' => $journalist,
								'datee' => $datee,
								'page' => $page,
								'mmcol' => $mmcol,
								'file_pdf' => $file_pdf , 
								'columne' => 0,  
								'circulation' => 0,  
								'size_jpeg' => 0,  
								'rate_bw' => 0,  
								'rate_fc' => 0,   
							)); 
		//echo $hasil;
		if($hasil > 0){ 
			
			$inputradio	= $this->radio->insert(array(
								'media_id' => $media_id,
								'title' => $title,
								'content' => $content,
								'journalist' => $journalist,
								'datee' => $datee,
								'timee' => $page,
								'duration' => $mmcol,
								'filee' => $file_pdf,  
								'article_id' => $hasil,   
								'createAt' => $createAt,   
							)); 
			 $out = array(
				'type'=>'success',
				'message'=>'Terima Kasih. Data Radio telah berhasil disimpan.'
			);
		}else {
			$out = array(
				'type'=>'error',
				'message'=>'Maaf. Penambahan media tidak berhasil kami simpan.'
			); 
		}
		
		$this -> output -> set_header("Pragma: no-cache");
        $this -> output -> set_header("Cache-Control: no-store, no-cache");
        $this -> output -> set_content_type('application/json') -> set_output(json_encode($out));
	}
	 
	 
	public function datatable(){
		//Important to NOT load the model and let the library load it instead.  
        $this -> load -> library('Datatable', array('model' => 'dtable_radio_model'));

        //format array is optional, but shown here for the sake of example
        $json = $this -> datatable -> datatableJson(
            array(
                'a_date_col' => 'date',
                'a_boolean_col' => 'boolean',
                'a_percent_col' => 'percent',
                'a_currency_col' => 'currency'
            )
        ); 

        $this -> output -> set_header("Pragma: no-cache");
        $this -> output -> set_header("Cache-Control: no-store, no-cache");
        $this -> output -> set_content_type('application/json') -> set_output(json_encode($json));

	}
	
} 
?>
