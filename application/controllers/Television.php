<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); 
/***************************************************************************************
NAME        : Television
PURPOSE     : -
REVISION    : -
***************************************************************************************/ 

Class Television extends MY_Controller {

    public function __construct() {
        parent::__construct();
        
        $this->load->model('media_model', 'media'); 
        $this->load->model('articles_model', 'articles'); 
        $this->load->model('television_model', 'television'); 
        $this->load->model('user_model', 'user');      
        
        $arrayCSS = array (						 
                        "asset/css/materialize.css",
						"asset/css/style.css",   
						"asset/js/plugins/perfect-scrollbar/perfect-scrollbar.css", 
						
                        'asset/css/notifIt.css',  
						'asset/css/jquery.dataTables.min.css',  
                        'asset/js/plugins/data-tables/css/jquery.dataTables.min.css', 
                        'asset/css/dataTables.tableTools.css',
                        'asset/css/materialize-tags.css',
                        'asset/css/jquery-te-1.4.0.css',
                    );
        
        $arrayJS = array (   
                        "asset/js/jquery-1.11.2.min.js",   

						"asset/js/materialize.min.js",
						 
						"asset/js/prism.js", 

						"asset/js/plugins/perfect-scrollbar/perfect-scrollbar.min.js", 
						"asset/js/plugins.js", 
						"asset/js/notifIt.js",
						
						'asset/js/plugins/datatables/js/jquery.dataTables.js',
						'asset/js/plugins/datatables/js/dataTables.tableTools.js', 
						'asset/js/materialize-tags.js', 
						'asset/js/jquery-te-1.4.0.min.js', 
						  
                    );
        
        $data['extraHeadContent'] = '';
        
        foreach ($arrayCSS as $css):
            $data['extraHeadContent'] .= '<link rel="stylesheet" href="'.base_url().$css.'">'.PHP_EOL;
        endforeach;
        
        foreach ($arrayJS as $js):
            $data['extraHeadContent'] .= '<script src="'.base_url().$js.'"></script>'.PHP_EOL;
        endforeach;
         $data['sub_menu'] = 'television';
         $data['group_menu'] = 'television';
         $data['statPage'] = 'television';
        $this->load->vars($data);
    }

    public function index() {  
		 
		$data['act']			  = 'create';	
		$data['content']   		  =  'television_form_view';
		
		$data['status'] 		  = 'add_artikel_tv';
		$data['title'] 		  	  = 'Input Berita Dari Television'; 
		$data['act']			  = 'create';	
		$data['url_action']		  = site_url('television/insert');
	
		$data['media'] = $this->media->get_many_by(array('media_type_id' => 12, 'statuse' => 'A')); 
		  
		$this->load->view('base/index', $data);   
    } 
    
     public function lists() {  
		 
		$data['group_menu']    = 'television_lists';
		$data['content']    = 'television_view';
        $this->load->view('base/index', $data);    
    } 
    
//     function get_api_token() {
//     	$ch = curl_init('http://10.10.10.4:8080/token');
//     	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
//     	curl_setopt($ch, CURLOPT_USERPWD, $username . ":" . $password);
//     	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//     	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
//     	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
//     	curl_setopt($ch, CURLOPT_HTTPHEADER, array(
//         	'Content-Type: application/json'
//     	    )
//     	);                                                         
// 	    $json = curl_exec($ch);
//    		$result = json_decode($json);
//    		$data = $result->data;
//    		return $data;
// 	}

// 	function api_to_insert_company($url, $data) {
// 
// 		$data_string = json_encode($data);
// 
// 		$ch = curl_init($url);
// 		curl_setopt($ch, CURLOPT_USERPWD, $username . ":" . $password);
// 		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
// 		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
// 		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
// 		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
// 		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
// 		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
// 			'Content-Type: application/json',
// 			'Content-Length: ' . strlen($data_string))
// 		);                                                         
// 		$json = curl_exec($ch);
// 		if (!empty($json)) {
// 			return TRUE;
// 		} else {
// 			return FALSE;
// 		}
// 	}
	
// function get_api_token() {
// 	$username = 'digivla';
// 	$password = 'nzVV2$/(zTH~>m3V';
//     $ch = curl_init('http://10.10.10.4:8080/token');
//     curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
//     curl_setopt($ch, CURLOPT_USERPWD, $username . ":" . $password);
//     curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//     curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
//     curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
//     curl_setopt($ch, CURLOPT_HTTPHEADER, array(
//         'Content-Type: application/json'
//         )
//     );                                                         
//     $json = curl_exec($ch);
//     $result = json_decode($json);
//     $data = $result->data;
//     return $data;
// }

	
	function insert(){ 
		 
		// echo "<pre>".var_export($_POST,1)."</pre>";
		 
		$media_id	=  $this->input->post('media_id');
		$title		=  $this->input->post('title');
		$content	=  $this->input->post('content');
		$journalist	=  $this->input->post('journalist');
		$datee		=  $this->input->post('datee');
		$page		=  $this->input->post('page');
		$mmcol		=  $this->input->post('mmcol');  
		
		$content	= strip_tags($content);
		
		$namefile='';
		if(isset($_FILES['file_pdf']['name']))
		{
			$namefile = basename($_FILES['file_pdf']['name']);
			$type = basename($_FILES['file_pdf']['type']); 
			$ext = pathinfo($namefile, PATHINFO_EXTENSION);
		}
		$stat_upl = true;
		$filename = '';
		if($namefile!='')
		{
			//2016-01-28
			//0123456789
			$tahun 	= substr($datee,0,4);
			$bulan 	= substr($datee,5,2);
			$hari 	= substr($datee,8,2);
			
			if (!file_exists('./media_tv/'.$tahun.'/')) {
				mkdir('./media_tv/'.$tahun.'/', 0777, true);				
			}
			if (!file_exists('./media_tv/'.$tahun.'/'.$bulan.'/')) {
					mkdir('./media_tv/'.$tahun.'/'.$bulan.'/', 0777, true);					
			}
			if (!file_exists('./media_tv/'.$tahun.'/'.$bulan.'/'.$hari.'/')) {
				mkdir('./media_tv/'.$tahun.'/'.$bulan.'/'.$hari.'/', 0777, true);
			}
					
			$media_id_str = str_pad($media_id, 3, 0, STR_PAD_LEFT);
			$time_number = time();
			$filename = ''.$tahun.'-'.$bulan.'-'.$hari.'-012-'.$media_id_str.'-'.$time_number .'.'.$ext.'';
			$folder = './media_tv/'.$tahun.'/'.$bulan.'/'.$hari.'/';
			$stat_upl = move_uploaded_file($_FILES['file_pdf']['tmp_name'], $folder.$filename);
			 
		}
		
		$file_pdf = ($filename == '') ? 'default.mp4' : $filename ; 
		$user_id  = $this->session->userdata('uid');
		$createAt	    	= date("Y-m-d H:i:s");
		$hasil 				= $this->articles->insert(array(
							'media_id' => $media_id,
							'title' => $title,
							'content' => $content,
							'journalist' => $journalist,
							'datee' => $datee,
							'page' => $page,
							'mmcol' => $mmcol,
							'file_pdf' => $file_pdf,  
							'columne' => 0,  
							'circulation' => 0,  
							'size_jpeg' => 0,  
							'rate_bw' => 0,  
							'rate_fc' => 0,
							'is_colour' => 0,
							'is_chart' => 0,
							'is_table' => 0
						)); 
		
		$data = array(
			'media_id' => $media_id,
			'title' => $title,
			'content' => $content,
			'journalist' => $journalist,
			'datee' => $datee.' '.$page.':00',
			'page' => $page,
			'mmcol' => $mmcol,
			'file_pdf' => $file_pdf,  
			'columne' => 0,  
			'circulation' => 0,  
			'size_jpeg' => 0,  
			'rate_bw' => 0,  
			'rate_fc' => 0,
			'is_colour' => 0,
			'is_chart' => 0,
			'is_table' => 0
		);
		
//		if($hasil>0){ 
			$q_create = $this->site_sentry->api_to_insert_article("http://172.16.0.4:8080/article/insert", $data);
			// $q_create = $this->api_to_insert_article("http://10.10.10.4:8080/article/insert_staging", $data); 
			// $x = $this->site_sentry->api_to_insert_article("http://10.10.10.4:8080/article/insert_staging", $data);
			
			$inputtv	= $this->television->insert(array(
				'media_id' => $media_id,
				'title' => $title,
				'content' => $content,
				'journalist' => $journalist,
				'datee' => $datee,
				'timee' => $page,
				'duration' => $mmcol,
				'filee' => $file_pdf,  
				'article_id' => $hasil,   
				'createAt' => $createAt,   
			)); 
			
			
			 $out = array(
				'type'=>'success',
				'message'=>'Terima Kasih. Data Television telah berhasil disimpan.'
			);
			
// 		}else {
// 			$out = array(
// 				'type'=>'error',
// 				'message'=>'Maaf. Penambahan media tidak berhasil kami simpan.'
// 			); 
// 		}
		
		$this -> output -> set_header("Pragma: no-cache");
        $this -> output -> set_header("Cache-Control: no-store, no-cache");
        $this -> output -> set_content_type('application/json') -> set_output(json_encode($out));
	}
	  
	 
	public function datatable(){
		//Important to NOT load the model and let the library load it instead.  
	 // add by asri
                if ($this->db->conn_id->ping() === FALSE)
                {
                        sleep(1);
                        $this->db->reconnect();
                }

        $this -> load -> library('Datatable', array('model' => 'dtable_television_model'));

        //format array is optional, but shown here for the sake of example
        $json = $this -> datatable -> datatableJson(
            array(
                'a_date_col' => 'date',
                'a_boolean_col' => 'boolean',
                'a_percent_col' => 'percent',
                'a_currency_col' => 'currency'
            )
        ); 

        $this -> output -> set_header("Pragma: no-cache");
        $this -> output -> set_header("Cache-Control: no-store, no-cache");
        $this -> output -> set_content_type('application/json') -> set_output(json_encode($json));

	}
	
} 
?>
