<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {
	
	public function __construct() {
		parent::__construct(); 
		
		$unlocked = array('auth','register','api' );
		if ( !$this->gate_keeper->is_logged_in() AND ! in_array(strtolower(get_class($this)), $unlocked)) {
			redirect('auth/login');
		}
	}
}


?>
