<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

Class Gate_keeper {

    public function Gate_keeper() {
        $this->obj =& get_instance();
    }
    
    public function is_logged_in() {
        if ($this->obj->session) {
            if ($this->obj->session->userdata('logged_in')) {
                return TRUE;
            }else{
                    return FALSE;
                }
        }else{
                return FALSE;
            }
    }
    
    private function get_user_id() {
        if ($this->is_logged_in()) {
            return $this->obj->session->userdata('uid');
        }
    }
    
    private function get_user_name() {
        if ($this->is_logged_in()) {
            return $this->obj->session->userdata('unm');
        }
    }
    
    private function get_user_level() {
        if ($this->is_logged_in()) {
            return $this->obj->session->userdata('ulevel');
        }
    }
    
    
    public function checkpriviledge(){ 
		 return '';
	}
    
    public function login_routine($usr_login, $password) {
        $this->obj->db->select('*'); 
        $this->obj->db->from('t_user u'); 
        $this->obj->db->join('m_level l','u.level_id = l.level_id');  
        $this->obj->db->where('u.user_email', $usr_login);
        $this->obj->db->where('u.user_pass', $password);
        $this->obj->db->where('u.user_status', 'D');
        
        $query = $this->obj->db->get();
        
        if ($query->num_rows() == 1) {
            $uid = $query->row()->user_id;
            $unm = $query->row()->user_nm;
            $ulv = $query->row()->level_id;
            $lnm = $query->row()->level_name;
            $user_img = $query->row()->user_img;
            $user_email = $query->row()->user_email; 
           
            $credentials = array('logged_in' => '1'
                                ,'uid' => $uid
                                ,'unm' => $unm
                                ,'ulevel' => $ulv
                                ,'lnm' => $lnm 
                                ,'uimg' => $user_img
                                ,'uemail' => $user_email
                                );
            $this->obj->session->set_userdata($credentials);
            log_message('info', $unm.' has logged in.');
            if ($ulv == 0) {
                redirect('home');
            }elseif ($ulv == 1) {
                redirect('home');
            }elseif ($ulv == 2 || $ulv == 3) {
                redirect('home');
            }else{ die('...o0O ||| LIMBO ||| O0o...');
            }
        }else{
                $this->logout_routine();
                redirect('auth/login_fail');
            }
    }
    
    public function logout_routine() {
        $credentials = array('logged_in','uid','unm','ulevel','lnm','uimg','uemail');
        $this->obj->session->unset_userdata($credentials);
        redirect('');
    }
    
    function date_indonesia() {
        $array_hari = array(1=>'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu', 'Minggu');
        $hari = $array_hari[date('N')];
        $tanggal = date ('j');
        $array_bulan = array(1=>'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');
        $bulan = $array_bulan[date('n')];
        $tahun = date('Y');
        return $hari.", ".$tanggal." ".$bulan." ".$tahun;
    }
	
	function insert_log($page) {
		$date 	= date('Y-m-d H:i:s');
		$ipne 	= $this->get_ip();
		$userid = $this->obj->session->userdata('uid');
        
		if ($userid!='') {
			$datane = array('log_userid'=>$userid,
                            'log_date'=>$date,
                            'log_visit'=>$page,
                            'log_ip'=>$ipne);
			$this->obj->db->insert('tb_logs', $datane);
		}
	}
	
	function get_ip() {
		if (isset($_SERVER["HTTP_X_FORWARDED_FOR"])) {
            $ips = $_SERVER["HTTP_X_FORWARDED_FOR"];
        }else{
				$ips = $_SERVER['REMOTE_ADDR'];
			}
        
		return $ips;
	}
}
?>
