<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

Class Site_sentry 
{
	private $username = 'digivla';
	private $password = 'nzVV2$/(zTH~>m3V';

	function Site_sentry()
	{
		$this->obj =& get_instance();
	}

	function is_logged_in()
	{
		if ($this->obj->session) {
			if ($this->obj->session->userdata('logged_in'))
			{
				return TRUE;
			}
			else
			{
				return FALSE;
			}
		}
		else
		{
			return FALSE;
		}		
	}
	
	function get_api_token() {
    	// $ch = curl_init('http://apps.antara-insight.id/token');
    	$ch = curl_init('http://172.16.0.4:8080/token');
    	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    	curl_setopt($ch, CURLOPT_USERPWD, $this->username . ":" . $this->password);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
	    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    	    'Content-Type: application/json'
        )
	    );                                                         
    	$json = curl_exec($ch);
	    $result = json_decode($json);
    	return $result;
  	}
  	
  	function insert_media($data){
  		$data_api_token = $this->get_api_token();
		$token = $data_api_token->data;

		$data_string = '{"token":"'.$token.'", ';
		$data_string .= '"media_id": '.$data['media_id'].', ';
		$data_string .= '"media_name": "'.$data['media_name'].'", ';
		$data_string .= '"media_type_id": '.$data['media_type_id'].', ';
		$data_string .= '"circulation": '.$data['circulation'].', ';
		$data_string .= '"rate_bw": '.$data['rate_bw'].', ';
		$data_string .= '"rate_fc": '.$data['rate_fc'].', ';
		$data_string .= '"language": "'.$data['language'].'", ';
		$data_string .= '"usere": "'.$data['usere'].'", ';
		$data_string .= '"input_date": "'.$data['input_date'].'", ';
		$data_string .= '"tier": '.$data['tier'].'} ';

		// $ch = curl_init('http://apps.antara-insight.id/tb_media/insert');
		$ch = curl_init('http://172.16.0.4:8080/tb_media/insert');
		curl_setopt($ch, CURLOPT_USERPWD, $this->username . ":" . $this->password);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Content-Type: application/json',
			'Content-Length: ' . strlen($data_string))
		);                                                         
		$json = curl_exec($ch);
		$result = json_decode($json);

		return $result->data;
  	}
  	
  	function update_media($data){
  		$data_api_token = $this->get_api_token();
		$token = $data_api_token->data;

		$data_string = '{"token":"'.$token.'", ';
		$data_string .= '"media_id": '.$data['media_id'].', ';
		$data_string .= '"media_name": "'.$data['media_name'].'", ';
		$data_string .= '"media_type_id": '.$data['media_type_id'].', ';
		$data_string .= '"circulation": '.$data['circulation'].', ';
		$data_string .= '"rate_bw": '.$data['rate_bw'].', ';
		$data_string .= '"rate_fc": '.$data['rate_fc'].', ';
		$data_string .= '"language": "'.$data['language'].'", ';
		$data_string .= '"usere": "'.$data['usere'].'", ';
		$data_string .= '"statuse": "'.$data['statuse'].'", ';
		$data_string .= '"tier": '.$data['tier'].'} ';

		// $ch = curl_init('http://apps.antara-insight.id/tb_media/update');
		$ch = curl_init('http://172.16.0.4:8080/tb_media/update');
		curl_setopt($ch, CURLOPT_USERPWD, $this->username . ":" . $this->password);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Content-Type: application/json',
			'Content-Length: ' . strlen($data_string))
		);                                                         
		$json = curl_exec($ch);
		$result = json_decode($json);

		return $result->data;
  	}
  	
  	function delete_media($data){
  		$data_api_token = $this->get_api_token();
		$token = $data_api_token->data;

		$data_string = '{"token":"'.$token.'", ';
		$data_string .= '"media_id": '.$data['media_id'].'}';

		// $ch = curl_init('http://apps.antara-insight.id/tb_media/delete');
		$ch = curl_init('http://172.16.0.4:8080/tb_media/delete');
		curl_setopt($ch, CURLOPT_USERPWD, $this->username . ":" . $this->password);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Content-Type: application/json',
			'Content-Length: ' . strlen($data_string))
		);                                                         
		$json = curl_exec($ch);
		$result = json_decode($json);

		return $result->data;
  	}
  	
  	function api_to_insert_article($url, $data) {
		$username = 'digivla';
		$password = 'nzVV2$/(zTH~>m3V';
		// $data_string = json_encode($data);
		// $data_string = '{"token":"';
		// $data_string .= get_api_token().'","client_name": "'.$data.'"}';
		$data_api_token = $this->get_api_token();
		$token = $data_api_token->data;
	
		$data_string	 = '{';
		$data_string	.= '"token": "'.$token.'",';
		$data_string	.= '"media_id": "'.$data['media_id'].'",';
		$data_string	.= '"title": "'.$data['title'].'",';
		$data_string	.= '"content": "'.$data['content'].'",';
		$data_string	.= '"journalist": "'.$data['journalist'].'",';
		$data_string	.= '"datee": "'.$data['datee'].'",';
		$data_string	.= '"page": "'.$data['page'].'",';
		$data_string	.= '"mmcol": "'.$data['mmcol'].'",';
		$data_string	.= '"file_pdf": "'.$data['file_pdf'].'",';
		$data_string	.= '"columne": "'.$data['columne'].'",';
		$data_string	.= '"circulation": "'.$data['circulation'].'",';
		$data_string	.= '"size_jpeg": "'.$data['size_jpeg'].'",';
		$data_string	.= '"rate_bw": "'.$data['rate_bw'].'",';
		$data_string	.= '"rate_fc": "'.$data['rate_fc'].'",';
		$data_string	.= '"is_colour": "'.$data['is_colour'].'",';
		$data_string	.= '"is_chart": "'.$data['is_chart'].'",';
		$data_string	.= '"is_table": "'.$data['is_table'].'"';
		$data_string	.= '}';
	

		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_USERPWD, $username . ":" . $password);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Content-Type: application/json',
			'Content-Length: ' . strlen($data_string))
		);                                                         
		$json = curl_exec($ch);
		$result = json_decode($json);
		$status = $result->status;
		return $result;
	}
}

?>
